import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MatMenuModule} from '@angular/material/menu';
import { MatIconModule} from '@angular/material/icon';
import { MatDividerModule} from '@angular/material/divider';
import { MatListModule} from '@angular/material/list';
import { MatSidenavModule} from '@angular/material/sidenav';
import { NavbarComponent } from './Pages/navbar/navbar.component';
import { FooterrComponent } from './Pages/footerr/footerr.component';




@Component({
  selector: 'app-root',
  standalone: true,
  imports: [FooterrComponent, NavbarComponent, RouterOutlet, MatToolbarModule, MatMenuModule, MatIconModule, MatDividerModule, MatListModule, MatSidenavModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {
  title = 'coboce-web';
}

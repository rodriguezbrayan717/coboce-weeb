import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotiCorreoComponent } from './noti-correo.component';

describe('NotiCorreoComponent', () => {
  let component: NotiCorreoComponent;
  let fixture: ComponentFixture<NotiCorreoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NotiCorreoComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NotiCorreoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

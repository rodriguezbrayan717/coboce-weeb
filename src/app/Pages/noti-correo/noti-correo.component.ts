import { Component } from '@angular/core';
import { NavbarComponent } from "../navbar/navbar.component";

@Component({
    selector: 'app-noti-correo',
    standalone: true,
    templateUrl: './noti-correo.component.html',
    styleUrl: './noti-correo.component.css',
    imports: [NavbarComponent]
})
export class NotiCorreoComponent {

}

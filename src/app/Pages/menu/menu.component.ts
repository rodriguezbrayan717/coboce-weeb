import { Component } from '@angular/core';
import { NavbarComponent } from "../navbar/navbar.component";


@Component({
    selector: 'app-menu',
    standalone: true,
    templateUrl: './menu.component.html',
    styleUrl: './menu.component.css',
    imports: [NavbarComponent]
})
export class MenuComponent {

}

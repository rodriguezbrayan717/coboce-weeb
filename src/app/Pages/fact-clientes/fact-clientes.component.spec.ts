import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FactClientesComponent } from './fact-clientes.component';

describe('FactClientesComponent', () => {
  let component: FactClientesComponent;
  let fixture: ComponentFixture<FactClientesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FactClientesComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FactClientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

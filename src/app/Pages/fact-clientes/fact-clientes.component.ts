import { Component } from '@angular/core';
import { NavbarComponent } from "../navbar/navbar.component";

@Component({
    selector: 'app-fact-clientes',
    standalone: true,
    templateUrl: './fact-clientes.component.html',
    styleUrl: './fact-clientes.component.css',
    imports: [NavbarComponent]
})
export class FactClientesComponent {

}

import { Routes } from '@angular/router';
import { LoginComponent } from './Pages/login/login.component';
import { MenuComponent } from './Pages/menu/menu.component';
import { FactClientesComponent } from './Pages/fact-clientes/fact-clientes.component';
import { NotiCorreoComponent } from './Pages/noti-correo/noti-correo.component';


export const routes: Routes = [
    {path: '', redirectTo:'login' , pathMatch:'full'},
    {path:'login', component:LoginComponent},
    {path: 'menu', component:MenuComponent},
    {path: 'factura-clientes', component:FactClientesComponent},
    {path: 'notificacion-correo', component:NotiCorreoComponent},
];
